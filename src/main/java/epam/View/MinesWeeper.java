package epam.View;

import epam.Controller.GameRunner;

public class MinesWeeper {
    public static void main(String[] args){
        GameRunner.init();
        GameRunner.test();
    }
}
