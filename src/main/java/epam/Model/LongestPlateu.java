package epam.Model;

import java.util.ArrayList;
import java.util.List;

public class LongestPlateu {
    public ArrayList<Integer> fillArr() {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1);
        arr.add(3);
        arr.add(5);
        arr.add(5);
        arr.add(5);
        arr.add(2);
        arr.add(1);
        arr.add(4);
        arr.add(7);
        return arr;
    }

    public List longestPlateu(ArrayList<Integer> arr) {
        ArrayList<Integer> longest = new ArrayList<Integer>();
        Integer max = arr.get(0);
        for (Integer i = 1; i < arr.size() - 2; i++) {
            if (arr.get(i) > max) {
                max = arr.get(i);
            }
        }
        for (Integer i = 1; i < arr.size() - 2; i++) {
            if (max == arr.get(i)) {
                longest.add(arr.get(i));
            }
        }
        return longest;
    }
}
