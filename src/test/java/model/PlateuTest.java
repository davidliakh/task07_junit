package model;

import epam.Model.LongestPlateu;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.RepeatedTest;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class PlateuTest {
    private static LongestPlateu plateu;

    @BeforeAll
    private static ArrayList makeList() {
        plateu = new LongestPlateu();
        ArrayList<Integer> list = new ArrayList<Integer>(plateu.fillArr());
        return list;
    }

    @Test
    public void isListEmpty() {
        assertEquals(plateu.fillArr().isEmpty(), false);
    }

    @Test
    public void longestTest() {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(5);
        arr.add(5);
        arr.add(5);
        Mockito.when(plateu.longestPlateu(makeList())).thenReturn(arr);
        assertEquals(plateu.longestPlateu(makeList()), arr);
    }

    @Test
    @RepeatedTest(3)
    public void longest() {
        final List<Integer> longest = plateu.fillArr();
        Assertions.assertTrue(longest.size() == 3);
    }
}
