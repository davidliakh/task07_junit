import controller.MinesWeeperTest;
import model.PlateuTest;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.runner.RunWith;

@SelectClasses({MinesWeeperTest.class, PlateuTest.class})
@RunWith(JUnitPlatform.class)
public class TestSuite {
}