package controller;

import epam.Model.Game;

import static org.junit.Assert.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MinesWeeperTest {
    private static Class clazz;
    private static Game game;
    private String[][] field = new String[12][12];

    @Mock
    private Game getTile;

    @BeforeAll
    public static void init() {
        game = new Game();
        clazz = game.getClass();
    }

    @Test
    public void updateTile() {
        Mockito.when(game.getTile(4, 5)).thenReturn(field[4][5]);
        assertEquals(game.getTile(4, 5), field[4][5]);
    }

    @Test
    public void protectedConstant() {
        assertEquals("*", game.mine);
    }

}
